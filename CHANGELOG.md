# Changelog

## Unreleased

### Fixed
- #1 : ajout de `selections editoriales` dans la liste des plugins utilisés dans `paquet.xml`

## 2024-07-03 - v1.0.0

### Featured
- pourvoir choisir plusieurs rubriques blog

### Fixed
- afficher la page plan
- liens plus visibles dans le contenu avec un soulignement

### Changed
- borne max 4.*
- passage en stable