# HTML5up Future Imperfect pour SPIP

Adaptation pour SPIP du squelette « Future Imperfect » de html5up https://html5up.net/future-imperfect

## Documentation

https://contrib.spip.net/XXXX

### Sidebar

Articles/rubriques mis en avant : Créer une sélection éditoriale avec l'identifiant `miniposts`
Menu de pied de page : Créer un menu avec l'identifiant `pied`

### Accueil

Menu d'entête : Créer un menu avec l'identifiant `entete`

### Divers

- le thème prend en charge les commentaires ainsi que les mots clefs
