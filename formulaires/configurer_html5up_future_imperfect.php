<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Formulaire de configuration,
 * déclaration des saisies.
 * @return array
 **/
function formulaires_configurer_html5up_future_imperfect_saisies_dist(): array {
	// $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
	$saisies = [
		'options' => [
			// Changer l'intitulé du bouton final de validation
			'texte_submit' => '<:bouton_enregistrer:>'
		],
		[ // Accueil
		'saisie' => 'fieldset',
			'options' => [
				'nom' => 'accueil',
				'label' => '<:public:accueil_site:>'
			],
			'saisies' => [
				// Article de l'accueil
				['saisie' => 'selecteur_rubrique',
					'options' => [
						'nom' => 'rubrique_blog',
						'label' => '<:html5up_future_imperfect:rubrique_blog:>',
						'explication' => '<:html5up_future_imperfect:rubrique_blog_explication:>',
						'multiple' => true
					]
				]
			]
		],
		[ // Configuration globale
		'saisie' => 'fieldset',
			'options' => [
				'nom' => 'configuration_theme',
				'label' => '<:icone_configuration_site:>'
			],
			'saisies' => [
				// Ne pas charger les polices du thème
				['saisie' => 'case',
					'options' => [
						'nom' => 'charger_polices',
						'label' => '<:html5up_future_imperfect:charger_polices:>',
						'label_case' => '<:html5up_future_imperfect:charger_polices_explication:>'
					]
				]
			]
		]
	
	];
	return $saisies;
}
